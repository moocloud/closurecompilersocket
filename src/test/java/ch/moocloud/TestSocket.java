package ch.moocloud;

import junit.framework.TestCase;
import org.newsclub.net.unix.AFUNIXSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class TestSocket extends TestCase {
    private Thread thread;

    public void setUp() {
        thread = new Thread(() -> Main.main(new String[]{"-v"}));
        thread.start();
    }

    public void testSocketCommunication() {
        try {
            AFUNIXSocket sock = initiateSocket();
            DataInputStream is = new DataInputStream(sock.getInputStream());

            writeFiles("examples/test.js", "examples/test.css", sock);

            List<String> files = new ArrayList<>();
            byte[] buf = new byte[2000];

            // We know that we're going to receive two files
            for (int i = 0; i < 2; i++) {
                int length = is.readInt();
                is.readFully(buf, 0, length);
                files.add(new String(buf, 0, length, StandardCharsets.UTF_8));
            }
            assertEquals(files.size(), 2);
            assertEquals(files.get(0), "function test(){console.log(\"ao\")};");
            assertEquals(files.get(1), ".lel{width:360px;height:360px}");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    public void testWithLargeCss() {
        for (int i = 0; i < 10; i++) {
            try {
                AFUNIXSocket sock = initiateSocket();
                DataInputStream is = new DataInputStream(sock.getInputStream());

                writeFiles("examples/test.js", "examples/style.css", sock);

                List<String> files = new ArrayList<>();

                // We know that we're going to receive two files
                for (int j = 0; j < 2; j++) {
                    int length = is.readInt();
                    byte[] buf = new byte[length];
                    is.readFully(buf, 0, length);
                    files.add(new String(buf, 0, length, StandardCharsets.UTF_8));
                }
                assertEquals(files.size(), 2);
                assertEquals(files.get(0).length(), 35);
                assertEquals(files.get(1).length(), 625141);
                files.forEach(System.out::println);
                is.close();
                sock.close();
            } catch (Exception e) {
                fail(e.getMessage());
            }
        }

    }

    private AFUNIXSocket initiateSocket() throws IOException {
        final File sockFile = new File("./closure.sock");

        // Wait a bit for socket to be created by the other thread
        if (!sockFile.exists()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                fail(e.getMessage());
            }
        }
        AFUNIXSocket sock = AFUNIXSocket.newInstance();
        sock.connect(new AFUNIXSocketAddress(sockFile));
        return sock;
    }

    private void writeFiles(String first, String second, AFUNIXSocket sock) throws IOException {
        DataOutputStream os = new DataOutputStream(sock.getOutputStream());

        String firstFile = readFile(first);
        String secondFile = readFile(second);

        os.writeByte('0');
        os.writeByte('J');
        os.writeInt(firstFile.length());
        os.writeBytes(firstFile);

        os.writeByte('1');
        os.writeByte('C');
        os.writeInt(secondFile.length());
        os.writeBytes(secondFile);
    }

    private String readFile(String path) throws IOException {
        String line;
        StringBuilder builder = new StringBuilder();
        BufferedReader b = new BufferedReader(new FileReader(path));
        while ((line = b.readLine()) != null) {
           builder.append(line);
        }
        return builder.toString();
    }

    public void tearDown() {
        thread.interrupt();
    }
}
