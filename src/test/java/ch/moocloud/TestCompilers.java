package ch.moocloud;

import com.google.common.css.JobDescription;
import com.google.common.css.JobDescriptionBuilder;
import com.google.common.css.SourceCode;
import com.google.common.css.compiler.ast.ErrorManager;
import com.google.common.css.compiler.ast.GssError;
import com.google.common.css.compiler.ast.GssParserException;
import junit.framework.TestCase;

public class TestCompilers extends TestCase {

    public void testJsCompiler() {
        String code = "//Random comment \nfunction test(){ console.log('test1'); }\n//other\nfunction boh(){ console.log('test2'); }";
        String s = JSCompiler.compile(code);
        assertEquals(s, "function test(){console.log(\"test1\")}function boh(){console.log(\"test2\")};");
    }

    public void testCssCompiler() {
        String code = ".test { \nwidth: 300px;\n height:300px}";
        JobDescription job = new JobDescriptionBuilder()
                .setOutputFormat(JobDescription.OutputFormat.COMPRESSED)
                .setEliminateDeadStyles(true)
                .setSimplifyCss(true)
                .setPreserveComments(false)
                .addInput(new SourceCode("test", code))
                .getJobDescription();

        try {
            String res = new CSSCompiler(job, e -> {
            }, new ErrorManager() {
                @Override
                public void report(GssError gssError) {

                }

                @Override
                public void reportWarning(GssError gssError) {

                }

                @Override
                public void generateReport() {

                }

                @Override
                public boolean hasErrors() {
                    return false;
                }
            }).compile();
            assertEquals(res, ".test{width:300px;height:300px}");
        } catch (GssParserException e) {
            fail(e.getMessage());
        }
    }
}
