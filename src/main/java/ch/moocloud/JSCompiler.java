package ch.moocloud;

import com.google.javascript.jscomp.*;
import com.google.javascript.jscomp.Compiler;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Simple class with a single static method to minify javascript
 * leveraging the Google Closure library
 */
public class JSCompiler {
    public static String compile(String jsCode) {
        Compiler.setLoggingLevel(Level.INFO);
        Compiler compiler = new Compiler();
        CompilerOptions options = new CompilerOptions();

        CompilationLevel.SIMPLE_OPTIMIZATIONS.setOptionsForCompilationLevel(options);
        WarningLevel.VERBOSE.setOptionsForWarningLevel(options);

        List<SourceFile> sources = new ArrayList<>();
        sources.add(SourceFile.fromCode("_", jsCode));

        compiler.compile(new ArrayList<>(), sources, options);
        return compiler.toSource();
    }
}
