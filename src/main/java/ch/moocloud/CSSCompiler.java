package ch.moocloud;

import com.google.common.css.ExitCodeHandler;
import com.google.common.css.JobDescription;
import com.google.common.css.JobDescriptionBuilder;
import com.google.common.css.SourceCode;
import com.google.common.css.compiler.ast.ErrorManager;
import com.google.common.css.compiler.ast.GssError;
import com.google.common.css.compiler.ast.GssParserException;
import com.google.common.css.compiler.commandline.DefaultCommandLineCompiler;


/**
 * Simple class with a single static method to minify css
 * leveraging the Google Closure library
 */
public class CSSCompiler extends DefaultCommandLineCompiler {
    protected CSSCompiler(JobDescription job, ExitCodeHandler exitCodeHandler, ErrorManager errorManager) {
        super(job, exitCodeHandler, errorManager);
    }

    public static String compile(String code) throws GssParserException{
        JobDescription job = new JobDescriptionBuilder()
                .setOutputFormat(JobDescription.OutputFormat.COMPRESSED)
                .setEliminateDeadStyles(true)
                .setSimplifyCss(true)
                .setPreserveComments(false)
                .addInput(new SourceCode("test", code))
                .getJobDescription();
            String res = new CSSCompiler(job, i -> {}, new ErrorManager() {
                @Override
                public void report(GssError gssError) {

                }

                @Override
                public void reportWarning(GssError gssError) {

                }

                @Override
                public void generateReport() {

                }

                @Override
                public boolean hasErrors() {
                    return false;
                }
            }).compile();
            return res;
    }
}
