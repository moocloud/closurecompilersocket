package ch.moocloud;

import com.google.common.css.compiler.ast.GssParserException;
import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import org.newsclub.net.unix.AFUNIXServerSocket;
import org.newsclub.net.unix.AFUNIXSocketAddress;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Util class to run a Unix socket: clients can send css and js files, this class will minify them through the
 * Google Closure libraries and send them back.
 * Communication protocol is as follows:
 * <h3>Client sending</h3>
 * Every time a client wants to send a file, it needs to begin communication by sending 6 bytes:
 * <ul>
 *     <li>First byte is 0 if other files are going to be sent later, 1 otherwise;</li>
 *     <li>Second byte is 'C' if this file is a CSS, 'J' if it's a JS;</li>
 *     <li>Remaining bytes are reserved for a 4-bytes (32 bit) integer indicating length of file</li>
 * </ul>
 * Client can then send its file, and repeat the process if it needs to send others.
 * <h3>Server sending</h3>
 * For each file the client has sent, server will initiate the communication by sending a 4-bytes integer indicating
 * the length of file about to be sent. The file is then sent to the client and the process is repeated for all files
 * client sent.
 * Server will first send JS files and then CSS files, in the order they were sent by the client
 * <h3>Errors</h3>
 * In case of errors (such as parsing JS or CSS errors), server will close the socket connection.
 */
public class SocketUtils {
    private static final int BUFFER_SIZE = 2000;

    private boolean verbose;
    private String socketPath;
    private String socketName;

    private ByteArrayBuffer buffer = new ByteArrayBuffer();
    private List<String> jsFiles = new ArrayList<>();
    private List<String> cssFiles = new ArrayList<>();

    public SocketUtils(boolean verbose, String socketPath, String socketName) {
        this.verbose = verbose;
        this.socketPath = socketPath;
        this.socketName = socketName;
        if (verbose)
            System.out.println("Socket will start in verbose mode");
    }

    public void startSocket() throws IOException {
        File sockFile = new File(socketPath, socketName);
        sockFile.deleteOnExit();
        AFUNIXServerSocket server = AFUNIXServerSocket.newInstance();
        server.bind(new AFUNIXSocketAddress(sockFile));
        while (!Thread.interrupted()) {
            if (verbose)
                System.out.println("Waiting for connection...");
            Socket socket = server.accept();
            if (verbose)
                System.out.println("Connected: " + socket);
            DataOutputStream os = new DataOutputStream(socket.getOutputStream());
            receiveFiles(socket);
            try {
                for (String minified : compileFromLists()) {
                    if (verbose)
                        System.out.println("Sending length " + minified.length());
                    os.writeInt(minified.length());
                    os.write(minified.getBytes(StandardCharsets.UTF_8));
                }
                jsFiles.clear();
                cssFiles.clear();
                socket.close();
            } catch (GssParserException e) {
                e.printStackTrace();
                socket.close();
            }
        }
    }

    private void receiveFiles(Socket socket) throws IOException{
        DataInputStream stream = new DataInputStream(socket.getInputStream());

        byte[] inBuf = new byte[BUFFER_SIZE];
        while (true) {
            if (stream.readByte() == '1') {
                if (verbose)
                    System.out.println("Reading final file");
                readAndAppendFile(inBuf, stream);
                stream.close();
                break;
            } else {
                if (verbose)
                    System.out.println("Reading file");
                readAndAppendFile(inBuf, stream);
            }
        }
    }

    // Read file sent from the client and append it to the
    // jsFiles or cssFiles accordingly
    private void readAndAppendFile(byte[] inBuf, DataInputStream stream) throws IOException{
        boolean isJs = stream.readByte() == 'J';
        int toBeRead = stream.readInt();

        // Keep reading until we can fit next stream.readFully call in our inBuf entirely
        while(toBeRead > BUFFER_SIZE) {
            stream.readFully(inBuf, 0, BUFFER_SIZE);
            toBeRead -= BUFFER_SIZE;
            buffer.write(inBuf);
        }

        stream.readFully(inBuf, 0, toBeRead);
        buffer.write(inBuf, 0, toBeRead);

        List<String> list = (isJs) ? jsFiles : cssFiles;
        list.add(new String(buffer.getRawData(), 0, buffer.size(), StandardCharsets.UTF_8));
        buffer.reset();
    }

    // For every file we got in our lists, we compile it with
    // the right minifier and append it to an array of Strings.
    // Notice that we first compile JS and then CSS, no matter what client
    // sent first.
    private String[] compileFromLists() throws GssParserException {
        String[] results = new String[jsFiles.size() + cssFiles.size()];
        int index = 0;
        for (String jsFile : jsFiles) {
            results[index] = JSCompiler.compile(jsFile);
            index++;
        }
        for (String cssFile : cssFiles) {
            results[index] = CSSCompiler.compile(cssFile);
            index++;
        }
        return results;
    }
}
