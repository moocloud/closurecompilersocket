package ch.moocloud;


import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;

public class Main {
    private static boolean verbose;
    private static String socketPath = "./";
    private static String socketName = "closure.sock";

    public static void main(String[] args) {
        try {
            if (!collectArgs(args))
                System.exit(1);
            new SocketUtils(verbose, socketPath, socketName)
                    .startSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean collectArgs(String[] args) {
        Iterator<String> iterator = Stream.of(args).iterator();
        while (iterator.hasNext()) {
            String[] argVal = iterator.next().split("=");

            switch (argVal[0]) {
                case "-v":
                case "--verbose":
                    verbose = true;
                    break;
                case "-s":
                    if (!iterator.hasNext()) {
                        System.out.println("Socket path was not specified!");
                        return false;
                    }
                    socketPath = iterator.next();
                    break;
                case "--socket-path":
                    if (argVal.length < 2){
                        System.out.println("Socket path was not specified!");
                        return false;
                    }
                    socketPath = argVal[1];
                    break;
                case "-S":
                    if (!iterator.hasNext()) {
                        System.out.println("Socket name was not specified!");
                    }
                    socketName = iterator.next();
                    break;
                case "--socket-name":
                    if (argVal.length < 2) {
                        System.out.println("Socket name was not specified!");
                    }
                    socketName = argVal[1];
                    break;
                case "-h":
                case "--help":
                    help();
                    return false;
                default:
                    System.out.println("Option " + argVal[0] + " was not recognized");
                    help();
                    return false;
            }
        }
        return true;
    }

    private static void help() {
        System.out.println(
                "ClosureCompilerSocket allows to minify JS and CSS files through a Unix Domain Socket. For more " +
                        "information, visit https://gitlab.com/moocloud/closurecompilersocket\n" +
                        "Usage: java -jar ClosureCompilerSocket.jar [OPTIONS]...\n" +
                        "Options:\n" +
                        "-v, --verbose\t\tPrint verbose output\n" +
                        "-s, --socket-path=PATH\tPath for the socket file\n" +
                        "-S, --socket-name=NAME\tName of the socket file\n" +
                        "-h, --help\t\tShow this help"
        );
    }
}
