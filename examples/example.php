<?php
// Create socket, getting ready for connection
$socket = socket_create(AF_UNIX, SOCK_STREAM, 0);

// Read files to be sent
$f = file_get_contents('test.js', 'rb');
$f2 = file_get_contents('test.css', 'rb');

// Connect to socket
// TODO check if connection was successful
$result = socket_connect($socket, "../closure.sock", 0);

// Send initiation sequence
socket_write($socket, "0J");
socket_write($socket, pack('N', strlen($f)));

// Send first file
socket_write($socket, $f);

// Send second initiation sequence
socket_write($socket, "1C");
socket_write($socket, pack('N', strlen($f2)));


// Send second file
socket_write($socket, $f2);

// Receive the compiled files, store them in an array and print them.

// Receive 4-bytes integer from socket
$length = socket_read($socket, 4);
while (strlen($length) < 4) {
    $length .= socket_read($socket, 4 - strlen($length));
}
$length = unpack('N', $length)[1];
echo "Length of incoming file: " . $length . "\n";

// Store first file in the array
$minf = array();
$data = socket_read($socket, $length);
$minf[0] = $data;

// Receive second file length
$length = socket_read($socket, 4);
while (strlen($length) < 4) {
    $length .= socket_read($socket, 4 - strlen($length));
}
$length = unpack('N', $length)[1];
echo "Length of incoming file: " . $length . "\n";

// Store second file in the array
$data = socket_read($socket, $length);
$minf[1] = $data;

print_r($minf);
?>