import socket
import time
import sys

s = socket.socket(socket.AF_UNIX)
s.connect('../closure.sock')
f = open(sys.argv[1], 'rb')
f2 = open(sys.argv[2], 'rb')

fr = f.read()
fr2 = f2.read()

s.send(b"0J")
s.send((len(fr)).to_bytes(4, 'big'))
s.send(fr)

s.send(b'1C')
s.send((len(fr2)).to_bytes(4, 'big'))
s.send(fr2)

d = ""
files = list()
while True:
    rcv = s.recv(4)
    if not rcv:
        break
    while (len(rcv) < 4):
        print("need to read again!")
        rcv += s.recv(4 - len(rcv))
    print(f"received: {rcv}")
    length = int.from_bytes(rcv, byteorder='big')
    print(f"receiving length: {length}")
    cf = s.recv(length)
    print(f"received file: {cf}")
    if not cf:
        break
    files.append(cf)

s.close()
f.close()
f2.close()
