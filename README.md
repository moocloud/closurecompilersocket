# Closure Compiler Socket
ClosureCompilerSocket is a Java program, packaged as a [jar](https://en.wikipedia.org/wiki/JAR_(file_format)), which
allows to minify Javascript and CSS files through a Unix domain socket. Minification leverages Google Closure compilers,
available [here](https://github.com/google/closure-compiler) and [here](https://github.com/google/closure-stylesheets),
whereas the Unix socket implementation was possible through [this](https://github.com/kohlschutter/junixsocket) library.  
You can download the latest jar in the root of the repository named as `ClosureCompilerSocket.jar`.  
This project has been developed by [Moocloud](https://moocloud.ch).

## Build and Run
The recommended way of building and running the project is to use [Maven](https://maven.apache.org/).
Remember to include the two Google's jar `closure-compiler-v*.jar` and `closure-stylesheets.jar` as 
project libraries.  
When you run the program you can pass several options: 
* `-v` or `--verbose` argument to have verbose output;
* `-s` or `--socket-path=PATH` to specify directory where socket will be placed;
* `-S` or `--socket-name=NAME` to specify the name of the socket. It will be combined with the previous
path, if given, or the current working directory;
* `-h` or `--help` show an help message;

## Socket communication
Socket communication happens through a Unix domain socket. Client can send as many files as it wishes to, CSS or JS.
Server will send the minified files back, JS files first and CSS after (the order of the files in the two groups remains
the same that the client sent).
For this communication to work, a little protocol has been implemented:  

### Client sending
Every time a client wants to send a file, it needs to begin communication by sending 6 bytes:
 * First byte is 0 if other files are going to be sent later, 1 otherwise;
 * Second byte is 'C' if this file is a CSS, 'J' if it's a JS;
 * Remaining bytes are reserved for a 4-bytes (32 bit) integer indicating length of file.  

Client can then send its file, and repeat the process if it needs to send others.

### Server sending
For each file the client has sent, server will initiate the communication by sending a **4-bytes integer** indicating
the length of file about to be sent.   
The file is then sent to the client and the process is repeated for all files
client sent.  
Server will first send JS files and then CSS files, in the order they were sent by the client.

### Errors
In case of errors (such as parsing JS or CSS errors), server will close the socket connection.

### Examples
You can find some examples in the `examples` folder in the root of the repository. Two very simple scripts are provided in Python and PHP.
Of course, you can find a similar client written in Java in the tests inside the source code.  

### Contributing
Feel free to contribute if you wish :D 

